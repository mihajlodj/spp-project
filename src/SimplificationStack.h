//============================================================================
// Name        : SimplificationStack.h
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : declares stack used to simplify graph coloring
//============================================================================
#pragma once
#include "IR.h"
#include <stack>
#include "Constants.h"
#include "InterferenceGraph.h"
using namespace std;

class SimplificationStack
{
public:
    std::stack<Variable*> stacc;
    SimplificationStack(InterferenceGraph& ig);
private:
    const int MAX_RANK = __REG_NUMBER__ - 1;
    int getVertexRank(int regnum, Graph& iGraph);
    int findVertexWithRank(int rank, Graph& iGraph);
};