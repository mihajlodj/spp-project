//============================================================================
// Name        : SimplificationStack.cpp
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : implements Simplification stack
//============================================================================
#include "SimplificationStack.h"

SimplificationStack::SimplificationStack(InterferenceGraph& ig)
{
    Graph iGraph = ig.graph;
    Variables iVars = ig.vars;
    Variables::iterator it;
    int highest_rank = MAX_RANK;
    while (!iGraph.empty())
    {
        if (highest_rank < 0)
        {
            throw std::runtime_error("Spill occured.");
        }

        int vertex = findVertexWithRank(highest_rank, iGraph);
        while (vertex != -1)
        {
            it = iVars.begin();
            advance(it, vertex);
            stacc.push(*it); // add variable to stack
            iVars.erase(it); // remove variable from list

            // remove graph vertex
            iGraph.erase(iGraph.begin() + vertex);
            for (int i = 0; i < (int)iGraph.size(); i++)
            {
                // remove edges connected to removed vertex
                iGraph[i].erase(iGraph[i].begin() + vertex);
            }
            vertex = findVertexWithRank(highest_rank, iGraph);
        }
        highest_rank--;
    }
    if (iVars.size() != 0)
    {
        throw std::runtime_error("Spill occured.");
    }
}

int SimplificationStack::getVertexRank(int reg, Graph& iGraph)
{
    // calculates rank of the variable with the position 'reg' in iGraph
    int rank = 0;
    // this shouldn't happen, but just to be sure
    if (iGraph.size() != iGraph[0].size()) { throw std::runtime_error("Interference matrix(graph) should be a square"); }
    for (int neighbor = 0; neighbor < (int)iGraph.size(); neighbor++)
    {
        int interferenceMark = iGraph[reg][neighbor];
        if (interferenceMark == __INTERFERENCE__)
            rank++;
    }
    return rank;
}

int SimplificationStack::findVertexWithRank(int rank, Graph& iGraph)
{
    /// <summary>
    /// finds whatever vertex has the rank equal to param "rank". Returns -1 if there is no such vertex
    /// </summary>
    /// <param name="rank">rank indicates number of node edges</param>
    /// <param name="iGraph">interference graph</param>
    /// <returns>index position of a variable (vertex of the graph)</returns>
    int retval = -1;
    for (int i = 0; i < (int)iGraph.size(); i++)
    {
        int currVertexRank = getVertexRank(i, iGraph);
        if (currVertexRank == rank)
        {
            retval = i;
            break;
        }
    }
    return retval;
}

