//============================================================================
// Name        : InterferenceGraph.h
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : declares Interference Matrix
//============================================================================
#pragma once
#include "IR.h"
#include "Constants.h"
#include <algorithm>
#include <iostream>

class InterferenceGraph
{
private:
    void buildMatrix();
public:
    InterferenceGraph(Instructions& instructions, Variables& regVariables, int memVarCount);
    void printInterferenceMatrix();
    Instructions ins;
    Variables vars; // vars in interference matrix and interference graph
    Graph graph; // interference graph
    
};