//============================================================================
// Name        : MIPSParser.
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : Declares class that parses .mavn code into variables and instructions
//============================================================================
#pragma once
#include "IR.h"
#include <string>

class MIPSParser
{
private:
    bool newLabel = false;
    int varPosCounter = 0;
    int lblPosCounter = 0;

    TokenList::iterator begin;
    TokenList::iterator iterator;
    TokenList::iterator end;
    TokenList tlist;
    Token getNextToken();
    Variable* getVariable(Token& currentToken);

    Labels labels;
    Variables vars;
    Instructions ins;

    Label getLabelByName(std::string lblname);
    Label getLabelByPos(int pos);

    bool isMEMNameValid(std::string name);
    bool isREGNameValid(std::string name);

    void makeMEMVar(Token& currentToken);
    void makeREGVar(Token& currentToken);
    void makeLABEL(Token& currentToken);

    Instruction* makeADDInstruction(Token& currentToken);
    Instruction* makeADDIInstruction(Token& currentToken);
    Instruction* makeSUBInstruction(Token& currentToken);
    Instruction* makeLAInstruction(Token& currentToken);
    Instruction* makeLWInstruction(Token& currentToken);
    Instruction* makeLIInstruction(Token& currentToken);
    Instruction* makeSWInstruction(Token& currentToken);
    Instruction* makeBInstruction(Token& currentToken);
    Instruction* makeBLTZInstruction(Token& currentToken);
    Instruction* makeNOPInstruction(Token& currentToken);
    Instruction* makeADDUInstruction(Token& currentToken);
    Instruction* makeDIVInstruction(Token& currentToken);
    Instruction* makeSEQInstruction(Token& currentToken);

    Instruction* makeInstruction(InstructionType type, Variables dests, Variables srcs, std::string inst_name);

    void setPredAndSucc();
public:
    Variables extractMEMVars();
    Variables extractREGVars();
    int getNumberOfREGVars();
    int getNumberOfMEMVars();
    Labels getLabels() { return labels; }
    Instructions getInstructions() { return ins; }

    MIPSParser(TokenList::iterator b, TokenList::iterator it, TokenList::iterator e, TokenList tl)
        : begin(b), iterator(it), end(e), tlist(tl) {}
    void Parse();
};