//============================================================================
// Name        : Alloc.h
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : declares namespace that contains functions for resource allocation
//============================================================================
#pragma once
#include "InterferenceGraph.h"
#include "SimplificationStack.h"
#include "Types.h"

namespace alloc
{
	int getColor(Variable* notColoredVariable, InterferenceGraph& ig, int memVarCount);
	bool resourceAllocation(SimplificationStack& ss, InterferenceGraph& ig, int memVarCount);

/**
 * Use this function to check resource allocation process.
 * This function goes thought variables list and interference graph
 * and check does variables which have interference in interference graph containe diferent regs.
 * @param ig pointer to interference graph
 * @return true if resource allocation is done with no error, else false
 */
	bool checkResourceAllocation(InterferenceGraph& ig, int memVarCount);
}