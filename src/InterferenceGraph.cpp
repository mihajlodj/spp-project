//============================================================================
// Name        : InterferenceGraph.cpp
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : implements interference graph
//============================================================================
#include "InterferenceGraph.h"

using namespace std;

InterferenceGraph::InterferenceGraph(Instructions& instructions, Variables& vars, int memVarCount)
{
    this->ins = instructions;
    this->vars = vars;
    buildMatrix();

    for (Instruction* x : instructions) {
        for (Variable* aliveVar : x->getOut()) {
            for (Variable* definedVar : x->getDef()) {
                if (aliveVar->getName() != definedVar->getName()
                    && aliveVar->getType() == Variable::VariableType::REG_VAR
                    && definedVar->getType() == Variable::VariableType::REG_VAR)
                {
                    graph[definedVar->getPosition() - memVarCount][aliveVar->getPosition() - memVarCount] = __INTERFERENCE__;
                    graph[aliveVar->getPosition() - memVarCount][definedVar->getPosition() - memVarCount] = __INTERFERENCE__;
                }
            }
        }
    }
}

void InterferenceGraph::buildMatrix()
{
    for (Variable* v : vars)
    {
        graph.push_back(std::vector<int>(vars.size(), __EMPTY__));
    }
}

void InterferenceGraph::printInterferenceMatrix()
{
    if (graph.size() == 0)
    {
        cout << "There is nothing to print!" << endl;
        return;
    }

    cout << "==========================================" << endl;
    cout << "Interference matrix:" << endl;
    cout << "==========================================" << endl;

    // print existing variables in order to mark rows and columns
    for (auto varIt = vars.begin(); varIt != vars.end(); varIt++)
    {
        cout << "\t" << (*varIt)->getName();
    }
    cout << endl;

    auto varIt = vars.begin();
    for (auto it1 = graph.begin(); it1 != graph.end(); it1++)
    {
        cout << (*varIt++)->getName();
        for (auto it2 = (*it1).begin(); it2 != (*it1).end(); it2++)
        {
            cout << "\t" << *it2;
        }
        cout << endl;
    }
    cout << "==========================================" << endl;
}