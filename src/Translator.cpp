//============================================================================
// Name        : Translator.cpp
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : implements translator class
//============================================================================
#include "Translator.h"

void Translator::translateVar(Variable* var)
{

    if (var->getType() == Variable::VariableType::REG_VAR)
    {
        translatedCode += "$t" + std::to_string(var->getAssignment() - 1);
    }
    else
    {
        translatedCode += var->getName();
    }
}

void Translator::translateInstruction(Instruction* i)
{
    InstructionType it = i->getType();
    Variables vars = i->extractVars();
    
    Variables::iterator iter = vars.begin();

    
    if (it == I_BLTZ || it == I_B) 
        // jumps first;
    {
        if (it == I_BLTZ)
        {
            translateVar(*iter);
            translatedCode += ", ";
        }
        translatedCode += i->getLabel().lbl_name;
        return;
    }
    else if (it == I_ADDI) 
        // num instruction with 3 arguments;
    {
        translateVar(*iter);
        translatedCode += ", ";
        iter++;
        translateVar(*iter);
        translatedCode += ", ";
        translatedCode += std::to_string(i->getNum());
        return;
    }
    else if (it == I_LI)
    { // num instruction with 2 arguments;
        translateVar(*iter);
        translatedCode += ", ";
        translatedCode += std::to_string(i->getNum());
        return;
    }

    int numOfVar = 0;
    for (Variable* v: vars)
    {
        if ((it == I_LW || it == I_SW) && numOfVar == 1) 
            // only paranthesis instructions
        {
            translatedCode += std::to_string(i->getNum());
            translatedCode += "(";
            translateVar(v);
            translatedCode += ")";
        }
        else
        {
            translateVar(v);
        }
        numOfVar++;

        if (numOfVar != vars.size()) {
            translatedCode += ", ";
        }
    }

    return;

}

Translator::Translator(Instructions& instructions, Variables& memVars, std::string filename)
{
    translatedCode = ".globl main\n.data\n";
    for (Variable* mv : memVars)
    {
        translatedCode += mv->getName() + ":\t.word\t" + std::to_string(mv->getValue()) + "\n";
    }

    translatedCode += "\n.text:\nmain:\n";
    for (Instruction* i : instructions)
    {
        translatedCode += "\t" + i->getName();
        translatedCode += "\t\t";
        translateInstruction(i);
        translatedCode += "\n";
    }

    std::cout << translatedCode << std::endl;

    std::ofstream assembly(filename + ".s");
    assembly << translatedCode;
    assembly.close();
}
