//============================================================================
// Name        : IR.h
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : declares instruction and variable classes
//============================================================================

#ifndef __IR__
#define __IR__

#include "Types.h"
#include "Token.h"
#include "SyntaxAnalysis.h"
#include <string>

typedef std::list<Token> TokenList;

typedef std::vector<std::vector<int>> Graph;

struct Label {
    std::string lbl_name = "";
    int lbl_pos = -1;
};

typedef std::vector<Label> Labels;
/**
 * This class represents one variable from program code.
 */
class Variable
{
public:
    enum VariableType
    {
        MEM_VAR,
        REG_VAR,
        NO_TYPE
    };
    // setters
    void setAssignment(Regs assignment) { m_assignment = assignment; }
    void setRegVar();
    void setNoType();
    void setNoAssign();
    void setMemVar();
    void setValue(int val);
    void setPosition(int pos);

    // getters
    int getValue();
    std::string getName();
    int getPosition();
    VariableType getType();
    Regs getAssignment();

    Variable() : m_type(NO_TYPE), m_name(""), m_position(-1), m_assignment(no_assign) {}
    Variable(std::string name, int pos) : m_type(NO_TYPE), m_name(name), m_position(pos), m_assignment(no_assign) {}
    Variable(std::string name, VariableType type) : m_type(type), m_name(name), m_position(-1), m_assignment(no_assign) {}
    Variable(std::string name, int pos, VariableType type) : m_type(type), m_name(name), m_position(pos), m_assignment(no_assign) {}

private:
    int value;
    VariableType m_type;
    std::string m_name;
    int m_position;
    Regs m_assignment;
};


/**
 * This type represents list of variables from program code.
 */
typedef std::list<Variable*> Variables;

/**
 * This class represents one instruction in program code.
 */
class Instruction
{
public:
    Instruction() : m_type(I_NO_TYPE) {};
    Instruction(InstructionType type, Variables& dst, Variables& src, std::string inst_name) :
        m_type(type), m_dst(dst), m_src(src), instruction_name(inst_name) {};
    
    void addDstVar(Variable* var);
    void addSrcVar(Variable* var);
    void addUseVar(Variable* var);
    void addDefVar(Variable* var);
    void addInVar (Variable* var);
    void addOutVar(Variable* var);

    void addSuccInst(Instruction* succ);
    void addPredInst(Instruction* pred);

    void setLabel(Label l);
    void setNum(int n);

    Variables& getDst();
    Variables& getSrc();
    Variables& getUse();
    Variables& getDef();
    Variables& getIn();
    Variables& getOut();
    Variables extractVars();

    std::string getName();
    InstructionType getType();
    int getNum();
    std::vector<Instruction*>& getSucc();
    std::vector<Instruction*>& getPred();
    Label getLabel();
    
    bool isNumInst = false; // I_LA, I_LW
    bool isNewLabl = false; // I_B,  I_BLTZ
    
private:
    std::string instruction_name;
    int num;
    Label m_label;
    InstructionType m_type;

    Variables m_dst;
    Variables m_src;

    Variables m_use;
    Variables m_def;
    Variables m_in;
    Variables m_out;
    std::vector<Instruction*> m_succ;
    std::vector<Instruction*> m_pred;
};

// determines instruction type through token type.
InstructionType identifyInstruction(Token t);

/**
 * This type represents list of instructions from program code.
 */
typedef std::vector<Instruction*> Instructions;

#endif
