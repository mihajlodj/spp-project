//============================================================================
// Name        : IR.cpp
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : implements Variable and List classes
//============================================================================
#include "IR.h"
#include "LexicalAnalysis.h"

// -------------------------- variable methods ----------------------------------

// determines instruction type through token type.
InstructionType identifyInstruction(Token t)
{
    switch (t.getType()) 
    {
    case T_ADD:				return I_ADD;
    case T_ADDI:			return I_ADDI;
    case T_ADDU:			return I_ADDU;
    case T_SUB:				return I_SUB;
    case T_LA:				return I_LA;
    case T_LI:				return I_LI;
    case T_LW:				return I_LW;
    case T_SW:				return I_SW;
    case T_BLTZ:			return I_BLTZ;
    case T_B:				return I_B;
    case T_NOP:				return I_NOP;
    case T_SEQ:				return I_SEQ;
    case T_DIV:				return I_DIV;
    default:				return I_NO_TYPE;
    }
}

void Variable::setValue(int val)
{
    value = val;
}

void Variable::setMemVar()
{
    m_type = MEM_VAR;
}

void Variable::setRegVar()
{
    m_type = REG_VAR;
}

void Variable::setNoType()
{
    m_type = NO_TYPE;
}

void Variable::setNoAssign() {
    m_assignment = no_assign;
}

void Variable::setPosition(int pos)
{
    m_position = pos;
}

int Variable::getValue() 
{
    return value;
}

std::string Variable::getName()
{
    return m_name;
}

int Variable::getPosition() 
{
    return m_position;
}

Variable::VariableType Variable::getType()
{
    return m_type;
}

Regs Variable::getAssignment()
{
    return m_assignment;
}


// ----------------- instruction methods -------------------------------------

void Instruction::addSuccInst(Instruction* i)
{
    m_succ.push_back(i);
}

void Instruction::addPredInst(Instruction* i)
{
    m_pred.push_back(i);
}

void Instruction::addOutVar(Variable* v)
{
    m_out.push_back(v);
}

void Instruction::addInVar(Variable* v)
{
    m_in.push_back(v);
}

std::string Instruction::getName()
{
    return instruction_name;
}

InstructionType Instruction::getType()
{
    return m_type;
}

Variables& Instruction::getDst()
{
    return m_dst;
}

Variables& Instruction::getSrc()
{
    return m_src;
}

Label Instruction::getLabel()
{
    return m_label;
}

Variables& Instruction::getUse()
{
    return m_use;
}

Variables& Instruction::getDef()
{
    return m_def;
}

Variables& Instruction::getIn()
{
    return m_in;
}

Variables& Instruction::getOut()
{
    return m_out;
}

Variables Instruction::extractVars()
{
    Variables vars;
    for (Variable* v1 : this->getDst())
    {
        vars.push_back(v1);
    }
    for (Variable* v2 : this->getSrc())
    {
        vars.push_back(v2);
    }
    return vars;
}

Instructions& Instruction::getSucc()
{
    return m_succ;
}

Instructions& Instruction::getPred()
{
    return m_pred;
}

void Instruction::addDefVar(Variable* v)
{
    m_def.push_back(v);
}

void Instruction::addDstVar(Variable* var)
{
    this->m_dst.push_back(var);
}

void Instruction::addSrcVar(Variable* var)
{
    this->m_src.push_back(var);
}

void Instruction::addUseVar(Variable* v)
{
    m_use.push_back(v);
}

void Instruction::setLabel(Label lbl)
{
    m_label = lbl;
}

int Instruction::getNum()
{
    return num;
}

void Instruction::setNum(int n)
{
    num = n;
    isNumInst = true;
}