//============================================================================
// Name        : Alloc.cpp
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : defines functions for resource allocation
//============================================================================
#include "Alloc.h"

Variables save;

int alloc::getColor(Variable* notColoredVariable, InterferenceGraph& ig, int memVarCount)
{
    Variables::iterator iter;
    Variables temp;

    // get varibales from stack which are interference with notColoredVariable
    for (iter = save.begin(); iter != save.end(); iter++)
    {
        Variable* variable = *iter;

        if (ig.graph[notColoredVariable->getPosition() - memVarCount][variable->getPosition() - memVarCount] == __INTERFERENCE__)
        {
            temp.push_back(variable);
        }
    }

    // find different color
    bool find;

    // starts from 1, because 0 is "no_assign"
    for (int color = 1; color < __REG_NUMBER__; color++) { 
        // starts from 1 because 0 is no_assign. 
        // we are assigning colors (registers) here
        find = true;

        for (iter = temp.begin(); iter != temp.end(); iter++) {
            if (color == (*iter)->getAssignment())
            {
                find = false;
            }
        }
        if (find == true)
            return color;
    }
    throw std::runtime_error("Actual spill occured");
}

bool alloc::resourceAllocation(SimplificationStack& ss, InterferenceGraph& ig, int memVarCount)
{
    Variable* topVar, * previusVar;

    previusVar = NULL;
    Regs reg = Regs::t0;
    while (ss.stacc.size() > 0) {

        topVar = ss.stacc.top();
        ss.stacc.pop();
        
        save.push_back(topVar);

        if (previusVar == NULL) {
            // beggining of the loop
            topVar->setAssignment(reg);
        }
        else {
            int color = alloc::getColor(topVar, ig, memVarCount);
            topVar->setAssignment((Regs)color);
        
        }

        previusVar = topVar;
    }

    ig.vars = save;
    return true;
}

bool alloc::checkResourceAllocation(InterferenceGraph& ig, int memVarCount)
{
    Variables::iterator it = ig.vars.begin();

    for (it; it != ig.vars.end(); it++)
    {
        Variables::iterator it2 = ig.vars.begin();
        for (it2; it2 != ig.vars.end(); it2++)
        {
            Regs assignment = (*it)->getAssignment();
            Regs assignment2 = (*it2)->getAssignment();
            if (assignment == assignment2 &&
                ig.graph[(*it)->getPosition() - memVarCount][(*it2)->getPosition() - memVarCount] == __INTERFERENCE__)
            {
                return false;
            }
        }
    }
    return true;;
}