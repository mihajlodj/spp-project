//============================================================================
// Name        : Types.h
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : declares enum types
//============================================================================
#ifndef __TYPES__
#define __TYPES__

#include "Constants.h"

#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <stack>

/**
* Supported token types.
*/

/*
added as per project request:
set equal (non-arithmetic instruction)
- seq rdest, rsrc1, rsrc2
addu (without overflow)
- addu rd, rs, rt
divide (with overflow) rdest, rsc
- div rs, rt
*/
enum TokenType
{
    T_NO_TYPE,

    T_ID,			// abcd...
    T_M_ID,			// m123...
    T_R_ID,			// r123...
    T_NUM,			// 123...
    T_WHITE_SPACE,

    // reserved words
    T_MEM,			// _mem
    T_REG,			// _reg
    T_FUNC,			// _func
    T_ADD,			// add
    T_ADDI,			// addi
    T_ADDU,			// addu
    T_SUB,			// sub
    T_LA,			// la
    T_LI,			// li
    T_LW,			// lw
    T_SW,			// sw
    T_BLTZ,			// bltz
    T_B,			// b
    T_NOP,			// nop
    T_SEQ,			// seq
    T_DIV,			// div


    // operators
    T_COMMA,		//,
    T_L_PARENT,		//(
    T_R_PARENT,		//)
    T_COL,			//:
    T_SEMI_COL,		//;

    // utility
    T_COMMENT,
    T_END_OF_FILE,
    T_ERROR,
};


/**
 * Instruction type.
 */
enum InstructionType
{
    I_NO_TYPE = 0,
    I_ADD,
    I_ADDI,
    I_ADDU,
    I_SUB,
    I_LA,
    I_LI,
    I_LW,
    I_SW,
    I_BLTZ,
    I_B,
    I_NOP,
    I_SEQ,  // non-arithmetic instruction. as per project request.
    I_DIV
};

/**
 * Reg names.
 */
enum Regs
{
    no_assign = 0,
    t0,
    t1,
    t2,
    t3
};

#endif
