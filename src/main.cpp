//============================================================================
// Name        : main.cpp
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : Main
//============================================================================

#include <iostream>
#include <exception>
#include "SyntaxAnalysis.h"
#include "LexicalAnalysis.h"
#include "IR.h"
#include "MIPSParser.h"
#include "InterferenceGraph.h"
#include <stack>
#include "SimplificationStack.h"
#include "Alloc.h"
#include "Translator.h"

using namespace std;

bool doesVarExist(Variable* var, Variables& vars)
{
    for (Variable* v : vars)
    {
        if (v->getName() == var->getName())
        {
            return true;
        }
    }

    return false;
}

void doLivenessAnalysis(Instructions& instructions)
{
    /*
    foreach n:in[n] <- 0; out[n] <- 0
    repeat
        foreach n:
            in'[n] <- in[n]
            out'[n] <- out[n]
            in[n] <- use[n] U (out[n] - def[n])

            out[n] <- (s_E_succ[n]) U in[s]
        until in' = in ^ out' = out


    Pseudocode by:
    https://lambda.uta.edu/cse5317/notes/node40.html
    */

    for (Instruction* instr : instructions) {
        instr->getIn().clear();
        instr->getOut().clear();
    }

    Variables in_;
    Variables out_;
    Instruction* instr = nullptr;
    int x = 0;
    do
    {
        for (Instructions::reverse_iterator it = instructions.rbegin(); it != instructions.rend(); it++)
        {

            instr = *it;

            // in'[n] <- in[n]
            in_ = instr->getIn();
            // out'[n] <- out[n]
            out_ = instr->getOut();


            //in[n] <- use[n] U (out[n] - def[n])
            for (Variable* var : instr->getUse())
            {
                instr->addInVar(var);
            }
            for (Variable* var : instr->getOut())
            {
                if (!doesVarExist(var, instr->getDef())) 
                {
                    instr->addInVar(var);
                }
            }

            //out[n] <- (s_E_succ[n]) U in[s]
            for (Instruction* succ : instr->getSucc())
            {
                for (Variable* var : succ->getIn())
                {
                    instr->addOutVar(var);
                }
            }

            instr->getOut().sort();
            instr->getOut().unique();
            instr->getIn().sort();
            instr->getIn().unique();

        }
    } while (!(instr->getIn() == in_ && instr->getOut() == out_));
}

int main()
{
    try
    {
        std::string file = "addu";
        std::string fileName = ".\\..\\examples\\" + file + ".mavn";
        bool retVal = false;

        LexicalAnalysis lex;

        if (!lex.readInputFile(fileName))
        {
            throw runtime_error("\nException! Failed to open input file!\n");
        }

        lex.initialize();

        retVal = lex.Do();

        if (retVal)
        {
            cout << "Lexical analysis finished successfully!" << endl;
            lex.printTokens();
        }
        else
        {
            lex.printLexError();
            throw runtime_error("\nException! Lexical analysis failed!\n");
        }
        
        // do: syntax analysis.
        SyntaxAnalysis syntax(lex);

        if (syntax.Do())
        {
            cout << "Syntax analysis finished successfully!" << endl;
        }
        else
        {
            throw runtime_error("\nSyntax analysis failed!\n");
        }

        TokenList tlist = lex.getTokenList();
        TokenList::iterator it_begin = tlist.begin();
        TokenList::iterator it_end = tlist.end();
    
        // Parse tokens
        // get instructions
        MIPSParser im(it_begin, it_begin, it_end, tlist);
        im.Parse();

        // do: LivenessAnalysis
        doLivenessAnalysis(im.getInstructions());
        cout << "Liveness Analysis finished successfully!" << endl;

        // interferenceGraph
        int memVarCount = im.getNumberOfMEMVars();
        InterferenceGraph ig(im.getInstructions(), im.extractREGVars(), memVarCount);
        ig.printInterferenceMatrix();

        // simplification stack, simplify
        SimplificationStack ss(ig);


        if (alloc::resourceAllocation(ss, ig, memVarCount))
        {
            if (alloc::checkResourceAllocation(ig, memVarCount))
            {
                cout << "\nResources allocated correctly.\n" << endl;
                Translator tr(im.getInstructions(), im.extractMEMVars(), file);
            }
            else
            {
                throw std::runtime_error("\nIncorrect allocatio\n");
            }
        }
        else
        {
            throw std::runtime_error("\nActual spill.\n");
        }
    }
    catch (runtime_error e)
    {
        cout << e.what() << endl;
        return 1;
    }

    return 0;
}
