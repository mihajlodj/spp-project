//============================================================================
// Name        : MIPSParser.cpp
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : Implements mipsparser that creates instruction objects
//============================================================================
#include "MIPSParser.h"
#include <iostream>

Token MIPSParser::getNextToken()
{
    return *iterator++;
}

Variable* MIPSParser::getVariable(Token& currentToken)
{
    for (Variable* v : vars)
    {
        if (v->getName() == currentToken.getValue())
            return v;
    }

    throw std::runtime_error("An undefined variable was used");
}

bool MIPSParser::isMEMNameValid(std::string name)
{
    // checks if var starts with 'm', as it's supposed to
    if (name.at(0) != 'm') return false;

    // checks for duplicates
    for (Variable* m : vars)
    {
        if (m->getName() == name) return false;
    }
    return true;
}

bool MIPSParser::isREGNameValid(std::string name)
{
    // checks if var starts with 'r', as it's supposed to
    if (name.at(0) != 'r') return false;

    // checks for duplicates
    for (Variable* m : vars)
    {
        if (m->getName() == name) return false;
    }
    return true;
}

void MIPSParser::makeMEMVar(Token& currentToken)
{
    // this constructor has default pos value -1, because this is a mem variable
    Variable* var = new Variable(currentToken.getValue(), Variable::VariableType::MEM_VAR);

    if (!isMEMNameValid(currentToken.getValue()))
    {
        throw std::runtime_error("There are 2 or more _mem variables with the same name OR there is a variable with an invalid name");
    }

    var->setPosition(varPosCounter);
    varPosCounter++;

    // next token is value of the mem_var
    // this is certain because lexical and syntax analysis finished successfully
    currentToken = getNextToken();
    var->setValue(std::stoi(currentToken.getValue()));
    vars.push_back(var);
}

void MIPSParser::makeREGVar(Token& currentToken)
{
    Variable* var = new Variable(currentToken.getValue(), Variable::VariableType::REG_VAR);

    if (!isREGNameValid(currentToken.getValue()))
    {
        std::cout << currentToken.getValue() << std::endl;
        throw std::runtime_error("There are 2 or more _reg variables with the same name OR there is a variable with an invalid name");
    }

    var->setPosition(varPosCounter);
    varPosCounter++;
    vars.push_back(var);
}

Instruction* MIPSParser::makeADDInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //add rid, rid, rid
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    return makeInstruction(InstructionType::I_ADD, dests, srcs, inst_name);
}

Instruction* MIPSParser::makeADDIInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //addi rid, rid, num
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // num

    Instruction* i = makeInstruction(InstructionType::I_ADDI, dests, srcs, inst_name);
    i->setNum(stoi(currentToken.getValue()));
    return i;
}

Instruction* MIPSParser::makeSUBInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //sub rid, rid, rid
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    return makeInstruction(InstructionType::I_SUB, dests, srcs, inst_name);
}

Instruction* MIPSParser::makeLAInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //sub rid, mid
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    return makeInstruction(InstructionType::I_LA, dests, srcs, inst_name);
}

Instruction* MIPSParser::makeLWInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //lw rid, num(rid)
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // num
    int num = stoi(currentToken.getValue());
    currentToken = getNextToken(); // (

    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // )

    Instruction* i = makeInstruction(InstructionType::I_LW, dests, srcs, inst_name);
    i->setNum(num);
    return i;
}

Instruction* MIPSParser::makeLIInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //li rid, num
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // num
    int num = stoi(currentToken.getValue());

    Instruction* i = makeInstruction(InstructionType::I_LI, dests, srcs, inst_name);
    i->setNum(num);
    return i;
}

Instruction* MIPSParser::makeSWInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //sw rid, num(rid)
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // num
    int num = stoi(currentToken.getValue());
    currentToken = getNextToken(); // (

    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // )

    Instruction* i = makeInstruction(InstructionType::I_SW, dests, srcs, inst_name);
    i->setNum(num);
    return i;
}

Instruction* MIPSParser::makeBInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //b id
    currentToken = getNextToken(); // name of label
    Label l = getLabelByName(currentToken.getValue());

    Instruction* i = makeInstruction(InstructionType::I_B, dests, srcs, inst_name);
    i->setLabel(l);
    return i;
}

Instruction* MIPSParser::makeBLTZInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //bltz rid, id(label)
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // name of label
    Label l = getLabelByName(currentToken.getValue());

    Instruction* i = makeInstruction(InstructionType::I_BLTZ, dests, srcs, inst_name);
    i->setLabel(l);
    return i;
}

Instruction* MIPSParser::makeNOPInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    return makeInstruction(InstructionType::I_NOP, dests, srcs, inst_name);
}

//added per project specifications

Instruction* MIPSParser::makeADDUInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //ADDU rid, rid
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    return makeInstruction(InstructionType::I_ADDU, dests, srcs, inst_name);
}

Instruction* MIPSParser::makeDIVInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //div (with overflow)
    //rid, rid (rdest, rsrc)
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    return makeInstruction(InstructionType::I_DIV, dests, srcs, inst_name);
}

Instruction* MIPSParser::makeSEQInstruction(Token& currentToken)
{
    Variables dests, srcs;
    std::string inst_name = currentToken.getValue();
    //seq rid, id
    currentToken = getNextToken(); // dest
    dests.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    currentToken = getNextToken(); // comma
    currentToken = getNextToken(); // src
    srcs.push_back(getVariable(currentToken));

    return  makeInstruction(InstructionType::I_SEQ, dests, srcs, inst_name);
}

void MIPSParser::Parse()
{
    Token currentToken = *begin;

    while (iterator != end)
    {
        switch (currentToken.getType())
        {
        case T_M_ID:
            makeMEMVar(currentToken);
            break;
        case T_R_ID:
            makeREGVar(currentToken);
            break;
        case T_ID:
            makeLABEL(currentToken);
            break;

        case T_ADD:
            ins.push_back(makeADDInstruction(currentToken));
            break;
        case T_ADDI:
            ins.push_back(makeADDIInstruction(currentToken));
            break;
        case T_ADDU:
            ins.push_back(makeADDUInstruction(currentToken));
            break;
        case T_B:
            ins.push_back(makeBInstruction(currentToken));
            break;
        case T_BLTZ:
            ins.push_back(makeBLTZInstruction(currentToken));
            break;
        case T_DIV:
            ins.push_back(makeDIVInstruction(currentToken));
            break;
        case T_LA:
            ins.push_back(makeLAInstruction(currentToken));
            break;
        case T_LI:
            ins.push_back(makeLIInstruction(currentToken));
            break;
        case T_LW:
            ins.push_back(makeLWInstruction(currentToken));
            break;
        case T_NOP:
            ins.push_back(makeNOPInstruction(currentToken));
            break;
        case T_SEQ:
            ins.push_back(makeSEQInstruction(currentToken));
            break;
        case T_SUB:
            ins.push_back(makeSUBInstruction(currentToken));
            break;
        case T_SW:
            ins.push_back(makeSWInstruction(currentToken));
            break;
        default: break;
        }

        currentToken = getNextToken();
    }

    setPredAndSucc();
}

Instruction* MIPSParser::makeInstruction(InstructionType type, Variables dests, Variables srcs, std::string inst_name)
{
    Instruction* i = new Instruction(type, dests, srcs, inst_name);

    for (Variable* v : dests)
    {
        i->addDefVar(v);
    }
    for (Variable* v : srcs)
    {
        i->addUseVar(v);
    }
    if (newLabel)
    {
        i->isNewLabl = true;
        newLabel = false;
    }
    lblPosCounter++;
    return i;
}

Label MIPSParser::getLabelByName(std::string lblname)
{
    for (Label lbl : labels)
    {
        if (lbl.lbl_name == lblname)
        {
            return lbl;
        }
    }

    throw std::runtime_error("Unknown label used.");
}

Label MIPSParser::getLabelByPos(int pos)
{
    for (Label lbl : labels)
    {
        if (lbl.lbl_pos == pos)
        {
            return lbl;
        }
    }

    throw std::runtime_error("Unknown label used.");
}

void MIPSParser::makeLABEL(Token& currentToken)
{
    Label l;
    l.lbl_name = currentToken.getValue();
    l.lbl_pos = lblPosCounter;
    labels.push_back(l);
    newLabel = true;
    
}

int MIPSParser::getNumberOfMEMVars()
{
    int retval = 0;
    for (Variable* v : vars)
    {
        if (v->getType() == Variable::VariableType::MEM_VAR) {
            retval++;
        }
    }
    return retval;
}

int MIPSParser::getNumberOfREGVars()
{
    int retval = 0;
    for (Variable* v : vars)
    {
        if (v->getType() == Variable::VariableType::REG_VAR) {
            retval++;
        }
    }
    return retval;
}
Variables MIPSParser::extractMEMVars()
{
    Variables retval;
    for (Variable* v : vars)
    {
        if (v->getType() == Variable::VariableType::MEM_VAR)
        {
            retval.push_back(v);
        }
    }
    return retval;
}
Variables MIPSParser::extractREGVars()
{
    Variables retval;
    for (Variable* v : vars)
    {
        if (v->getType() == Variable::VariableType::REG_VAR)
        {
            retval.push_back(v);
        }
    }
    return retval;
}

void MIPSParser::setPredAndSucc()
{
    int numOfInstructions = ins.size();

    //Instructions::iterator it;
    int index = 0; // index in the list represents the position
    for (index; index < numOfInstructions; index++)
    {
        Instruction* p = ins[index];

        if (index == 0)
        {
            p->addSuccInst(ins[index + 1]); // first instruction has no predecessors

        }

        else if (index == numOfInstructions - 1 || p->getType() == T_B) {
            p->addPredInst(ins[index - 1]); // last instruction has no successors
        }
        else {
            p->addPredInst(ins[index - 1]);
            p->addSuccInst(ins[index + 1]);

        }

        if (p->getType() == I_B || p->getType() == I_BLTZ)
        {
            Label l = p->getLabel();
            Instruction* branchLocation = ins[l.lbl_pos];
            p->addSuccInst(branchLocation);
            branchLocation->addPredInst(p);
        }
    }
}