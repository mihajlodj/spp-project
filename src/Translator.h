//============================================================================
// Name        : Translator.h
// Author      : Mihajlo Djordjevic
// Date        : 04.06.2021.
// Copyright   : 
// Description : declares class used for writing down assembly code
//============================================================================
#pragma once
#include "IR.h"
#include "InterferenceGraph.h"
#include <iostream>
#include <fstream>
#include <string>

class Translator
{
private:
    void translateVar(Variable* var);
    std::string translatedCode;
    void translateInstruction(Instruction* i);
public:
    Translator(Instructions& instructions, Variables& memVars, std::string filename);
};